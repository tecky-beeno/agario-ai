import { Cell } from './types';
import { cells, npcs } from './state';
export function removeArrayElement<T>(array: T[], element: T) {
  let idx = array.indexOf(element);
  if (idx == -1) return;
  array.splice(idx, 1);
}
export function removeCell(cell: Cell) {
  removeArrayElement(cells, cell);
  removeArrayElement(npcs, cell);
}
