import { Player, Position, room } from './game-state';
import { calcDistance } from './utils';

export type Session = {
  player: Player;
  socket: WebSocket;
};

export let sessions: Session[] = [];

let World = {
  width: 600,
  height: 600,
};

let server = new Server();
server.on('connection', (socket) => {
  let player: Player;
  socket.on('message', (data) => {
    let message = JSON.parse(data);
    switch (message.type) {
      case 'move':
        // max(-1, min(1, input))
        // e.g. input = 0.5; min(1, 0.5) -> 0.5; max(-1, 0.5) -> 0.5
        // e.g. input = 10; min(1, 10) -> 1
        // e.g. input = -10; min(1, -10) -> max(-1, -10) -> -1
        let dx = Math.max(-1, Math.min(1, message.dx));
        let dy = Math.max(-1, Math.min(1, message.dy));
        movePlayer(player, dx, dy);
        break;
      case 'join':
        player = {
          position: {
            x: Math.random() * World.width,
            y: Math.random() * World.height,
          },
          color: 'red',
          name: message.name,
          radius: 5,
        };
        let session: Session = {
          socket,
          player,
        };
        sessions.push(session);
        room.players.push(player);
        break;
    }
  });
});

export function movePlayer(player: Player, dx: number, dy: number) {
  player.position.x += dx;
  player.position.y += dy;
  let self = player;
  for (let session of sessions) {
    let other = session.player;
    if (other === self) {
      continue;
    }
    let distance = calcDistance(self.position, other.position);
    if (distance < self.radius) {
      // other die
    } else if (distance < other.radius) {
      // self die
    } else {
      // to far
    }
    session.socket.send(JSON.stringify({ type: 'move', player }));
  }
}
