import { width, height } from './state';

export let canvas: HTMLCanvasElement = document.querySelector('#world');
canvas.width = width;
canvas.height = height;

export let context = canvas.getContext('2d');