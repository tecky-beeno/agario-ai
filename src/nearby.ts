import { Cell } from './types';
import { cells } from './state';
import { calcDistance, rectToPolarAngle } from './math';


export function findNearByCells(
  self: Cell,
): Array<{
  distance: number;
  angle: number;
  cell: Cell;
}> {
  return cells
    .map((other) => {
      let distance = other == self ? -1 : calcDistance(self, other);
      return { cell: other, distance };
    })
    .filter((record) => record.distance != -1)
    .sort((a, b) => a.distance - b.distance)
    .slice(0, 5)
    .map(({ cell, distance }) => {
      return {
        distance,
        angle: rectToPolarAngle(self, cell),
        cell,
      };
    });
}
