import { Cell } from './types';
import { findCollision } from './collision';
import { removeCell } from './garbage-collector';

export function checkFight(self: Cell) {
  let other = findFightTarget(self);
  if (!other) return;
  fight(self, other);
}

export function findFightTarget(self: Cell) {
  let other = findCollision(self);
  if (!other) return;
  if (other.radius == self.radius) {
    return;
  }
  return other;
}

export function fight(a: Cell, b: Cell) {
  if (a.radius > b.radius) {
    // a is bigger
    eat(a, b);
  } else if (a.radius < b.radius) {
    // b is bigger
    eat(b, a);
  } else {
    // same size
    return;
  }
}

export function eat(attacker: Cell, defender: Cell) {
  defender.alive = false;
  attacker.radius += defender.radius / 2;
  setTimeout(() => {
      removeCell(defender)
  }, 5000);
}
