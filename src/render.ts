import { context } from './ui';
import { width, height, cells, mouseX, mouseY } from './state';

export function render() {
  // draw background
  context.fillStyle = '#c0ffee';
  context.fillRect(0, 0, width, height);

  // draw foods, NPC, and player
  for (let cell of cells) {
    context.fillStyle = cell.alive ? cell.color : 'grey';
    context.fillRect(
      cell.x - cell.radius,
      cell.y - cell.radius,
      cell.radius * 2,
      cell.radius * 2,
    );
  }

  // draw mouse cross
  context.fillStyle = 'black';
  let crossSize = 20;
  let crossWidth = 2;
  context.fillRect(
    mouseX - crossWidth,
    mouseY - crossSize,
    crossWidth * 2,
    crossSize * 2,
  );
  context.fillRect(
    mouseX - crossSize,
    mouseY - crossWidth,
    crossSize * 2,
    crossWidth * 2,
  );

  requestAnimationFrame(render);
  // setTimeout(render, 0);
}

requestAnimationFrame(render);
// setTimeout(render, 0);
