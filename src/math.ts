import { Cell } from './types';

export function calcDistance(a: Cell, b: Cell) {
  let x = a.x - b.x;
  let y = a.y - b.y;
  let distance = Math.sqrt(x * x + y * y);
  return distance;
}

export function rectToPolarAngle(self: Cell, other: Cell): number {
  let x = other.x - self.x;
  let y = other.y - self.y;
  if (x == 0 && y == 0) return 0;
  if (x == 0) {
    if (y > 0) return 0;
    if (y < 0) return Math.PI;
  }
  if (y == 0) {
    if (x > 0) return Math.PI / 2;
    if (x < 0) return Math.PI * 1.5;
  }
  let theta = Math.atan(y / x);
  if (x > 0 && y > 0) {
    return theta;
  }
  if (x > 0 && y < 0) {
    return theta + Math.PI;
  }
  if (x < 0 && y < 0) {
    return theta + Math.PI;
  }
  if (x < 0 && y > 0) {
    return theta + Math.PI * 2;
  }
  return theta;
}

// let self: Cell = {
//   x: 0,
//   y: 0,
// } as any;
// let other: Cell = {
//   x: 0,
//   y: 0,
// } as any;
// let theta = calcAngle(self, other);
// theta = (theta / Math.PI) * 180;
// console.log({ theta });
