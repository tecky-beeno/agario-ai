import { npcs, randomNewCell, cells, ais } from './state';
import { randomElement } from './random';
import { moveCell } from './move';
import { findNearByCells } from './nearby';

export function addAI() {
  let cell = randomNewCell(10, 'purple');
  cells.push(cell);
  ais.push(cell)
  findNearByCells
}

setTimeout(() => {
  for (let i = 0; i < 20; i++) {
    addAI();
  }
}, 2000);

type Model ={
  gene:number[]
  weights:number[][]
}

type Neuron = {
  bias:number
  connections:Connection[]
}

type Connection = {
  weight:number
  input:Neuron
  output:Neuron
}

function newModel(){
  // input is 5 near-by cells (radius, x, y)
}

export function moveAI() {
  for (let self of ais) {
    if (!self.alive) continue;
    let other = randomElement(cells);
    if (other == self) continue;
    if (other.radius > self.radius) {
      // other is bigger, escape
      let dx = other.x - self.x;
      let dy = other.y - self.y;
      let targetX = self.x - dx;
      let targetY = self.y - dy;
      moveCell(self, targetX, targetY);
    } else if (other.radius < self.radius) {
      // self is bigger, chase it
      moveCell(self, other.x, other.y);
    }
    // self.x += 5;
    // self.y += 5;
    // checkFight(self);
  }
}

setInterval(moveAI, 300);
