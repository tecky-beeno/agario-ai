import { canvas } from './ui';
import { Cell, Food, NPC, Player, AI } from './types';

export let width = document.body.clientWidth;
export let height = document.body.clientHeight;

export let cells: Cell[] = [];
export let npcs: NPC[] = [];
export let ais: AI[] = [];
export let player: Player = randomNewCell(20, 'orange');

cells.push(player);

export function randomColor(): string {
  return 'red';
}
export function randomNewCell(radius: number, color: string): Cell {
  return {
    x: Math.random() * width,
    y: Math.random() * height,
    color,
    radius,
    alive: true,
  };
}
window.addEventListener('resize', () => {
  width = document.body.clientWidth;
  height = document.body.clientHeight;
  canvas.width = width;
  canvas.height = height;
});

export let mouseX = width / 2;
export let mouseY = height / 2;
window.addEventListener('mousemove', (event) => {
  mouseX = event.clientX;
  mouseY = event.clientY;
});
