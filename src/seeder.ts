import { randomNewCell, cells } from './state';

export function addFood() {
  let cell = randomNewCell(5, 'green');
  cells.push(cell);
}

setInterval(addFood, 1000);

for (let i = 0; i < 5; i++) {
  addFood();
}
