import { player, mouseX, mouseY } from './state';
import { moveCell } from './move';
export function movePlayer() {
  moveCell(player, mouseX, mouseY);
}
setInterval(movePlayer, 30);
