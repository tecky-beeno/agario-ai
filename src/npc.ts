import { npcs, randomNewCell, cells } from './state';
import { randomElement } from './random';
import { moveCell } from './move';
export function addNPC() {
  let cell = randomNewCell(10, 'red');
  npcs.push(cell);
  cells.push(cell);
}
setInterval(addNPC, 5000);
export function moveNPC() {
  for (let self of npcs) {
    if (!self.alive) continue;
    let other = randomElement(cells);
    if (other == self) continue;
    if (other.radius > self.radius) {
      // other is bigger, escape
      let dx = other.x - self.x;
      let dy = other.y - self.y;
      let targetX = self.x - dx;
      let targetY = self.y - dy;
      moveCell(self, targetX, targetY);
    } else if (other.radius < self.radius) {
      // self is bigger, chase it
      moveCell(self, other.x, other.y);
    }
    // self.x += 5;
    // self.y += 5;
    // checkFight(self);
  }
}
setInterval(moveNPC, 300);
