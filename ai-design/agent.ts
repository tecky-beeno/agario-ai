import { Room } from './game-state';
import { Session, movePlayer } from './web-state';
import { calcDistance, compare } from './utils';
export type Agent = {
  session: Session;
  nn: NN;
  gene: Gene;
  score: number;
};
export type NN = {
  inputLayer: Layer;
  hiddenLayers: Layer[];
  outputLayer: Layer;
};
export type Layer = {
  nodes: Node[];
};
export type Node = {
  threshold: number;
  value: number;
  connections: Connection[];
};
export type Connection = {
  inputNode: Node;
  outputNode: Node;
  weight: number;
};

export type Gene = {
  chromosome: number[];
  fitness: number;
};

// valid range of value: -1 to 1, inclusive
export function createGeneFromNN(nn: NN): Gene {
  let chromosome: number[] = [];
  for (let node of nn.inputLayer.nodes) {
    chromosome.push(node.threshold);
  }
  for (let hiddenLayer of nn.hiddenLayers) {
    for (let node of hiddenLayer.nodes) {
      chromosome.push(node.threshold);
      for (let connection of node.connections) {
        chromosome.push(connection.weight);
      }
    }
  }
  for (let node of nn.outputLayer.nodes) {
    chromosome.push(node.threshold);
    for (let connection of node.connections) {
      chromosome.push(connection.weight);
    }
  }
  return { chromosome, fitness: 0 };
}

function updateNN(agent: Agent) {
  let nn = agent.nn;
  let chromosome = agent.gene.chromosome;
  let index = 0;
  for (let node of nn.inputLayer.nodes) {
    node.threshold = chromosome[index];
    index++;
  }
  for (let hiddenLayer of nn.hiddenLayers) {
    for (let node of hiddenLayer.nodes) {
      node.threshold = chromosome[index];
      index++;
      for (let connection of node.connections) {
        connection.weight = chromosome[index];
        index++;
      }
    }
  }
  for (let node of nn.outputLayer.nodes) {
    node.threshold = chromosome[index];
    index++;
    for (let connection of node.connections) {
      connection.weight = chromosome[index];
      index++;
    }
  }
}

function generateNextRound(agents: Agent[]) {
  let genes = agents.map((agent) => {
    agent.gene.fitness = agent.score;
    return agent.gene;
  });
  genes.sort((a, b) => -compare(a.fitness, b.fitness));
  let N = genes.length;
  // n/2_th to n_th
  for (let childIdx = N / 2; childIdx < N; childIdx++) {
    let parent1Idx = Math.floor((Math.random() * N) / 2); // 1_st to n/2_th
    let parent2Idx = Math.floor((Math.random() * N) / 2); // 1_st to n/2_th
    let parent1Gene = genes[parent1Idx];
    let parent2Gene = genes[parent2Idx];
    let childGene = genes[childIdx];
    // crossover
    for (let i = 0; i < childGene.chromosome.length; i++) {
      childGene.chromosome[i] =
        Math.random() < 0.5
          ? parent1Gene.chromosome[i]
          : parent2Gene.chromosome[i];
      // muation
      if (Math.random() < 2 / 100) {
        childGene.chromosome[i] = randomValue();
      }
    }
  }
  for (let i = 0; i < genes.length; i++) {
    let agent = agents[i];
    agent.gene = genes[i];
    updateNN(agent);
  }
}

// range from -1 to 1
function randomValue() {
  let r = Math.random(); // range from 0 to 1
  let r2 = r * 2; // range from 0 to 2
  let r3 = r2 - 1; // range from -1 to 1
  return r3;
}

function runAgent(agent: Agent, state: Room) {
  let player = agent.session.player;
  let points = [...state.foods, ...state.players].map((food) => {
    let distance = calcDistance(food.position, player.position);
    let sizeDiff = player.radius - food.radius;
    return { distance, sizeDiff };
  });
  points.sort((a, b) => compare(a.distance, b.distance));
  let closePoints = points.slice(0, 5);
  let inputs: number[] = [];
  for (let point of closePoints) {
    inputs.push(point.distance, point.sizeDiff);
  }
  let output = runNN(agent, inputs);
  let dx = output[0];
  let dy = output[1];
  movePlayer(player, dx, dy);
}

function runNN(agent: Agent, inputs: number[]): number[] {
  let nn = agent.nn;
  for (let i = 0; i < nn.inputLayer.nodes.length; i++) {
    nn.inputLayer.nodes[i].value = inputs[i];
  }
  for (let hiddenLayer of nn.hiddenLayers) {
    for (let node of hiddenLayer.nodes) {
      let sum = 0;
      for (let connection of node.connections) {
        sum += connection.inputNode.value * connection.weight;
      }
      node.value = nodeValue(sum - node.threshold);
    }
  }
  let outputs: number[] = nn.outputLayer.nodes.map((node) => {
    let sum = 0;
    for (let connection of node.connections) {
      sum += connection.inputNode.value * connection.weight;
    }
    node.value = nodeValue(sum - node.threshold);
    return node.value;
  });
  return outputs;
}

// input: unbounded
// output: range from -1 to 1
function nodeValue(input: number): number {
  let value = sigmoid(input); // range from 0 to 1
  return value * 2 - 1; // range from -1 to 1
}

// output range from 0 to 1
function sigmoid(x: number): number {
  return 1 / (1 + Math.exp(-x));
}
