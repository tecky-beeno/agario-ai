import { Cell } from './types';
import { cells } from './state';
import { calcDistance } from './math';

export function isCollide(a: Cell, b: Cell) {
  let distance = calcDistance(a, b);
  return distance < a.radius + b.radius;
}

export function findCollision(self: Cell) {
  for (let other of cells) {
    if (self === other) continue;
    if(!other.alive) continue
    if (isCollide(self, other)) {
      return other;
    }
  }
}
