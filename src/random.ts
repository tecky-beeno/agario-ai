export function randomElement<T>(array: T[]): T {
  let idx = Math.floor(Math.random() * array.length);
  return array[idx];
}
