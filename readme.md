# agar.io

Realtime game, inspired from https://agar.io/#ffa

## roadmap
- [x] draw cells
- [x] random place food
- [x] random move NPC
- [x] read user input (mouse, keyboard)
- [x] move player
- [x] detect collision
- [x] eating cell/food
- [x] grow cell
- [ ] online version
- [ ] multi-player
- [ ] extract data for AI trainning
- [ ] train AI (vs real players and NPC)
- [ ] turn some NPC into AI
- [ ] zoom out when player become bigger