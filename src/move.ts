import { Cell } from './types';
import { checkFight } from './fight';
export function moveCell(cell: Cell, targetX, targetY) {
  let dX = targetX - cell.x;
  let dY = targetY - cell.y;
  dX = Math.min(dX, 5);
  dX = Math.max(dX, -5);
  dY = Math.min(dY, 5);
  dY = Math.max(dY, -5);
  cell.x += dX;
  cell.y += dY;
  checkFight(cell);
}
