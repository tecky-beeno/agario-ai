// assume only one room
export type Room = {
  players: Player[];
  foods: Food[];
};

export type Player = {
  position: Position;
  color: Color;
  name: string;
  radius: number;
};

export type Food = {
  position: Position;
  color: Color;
  radius: number;
};

export type Position = {
  x: number;
  y: number;
};
export type Color = string; // e.g. #665544

export let room:Room={
  players:[],
  foods:[],
}
