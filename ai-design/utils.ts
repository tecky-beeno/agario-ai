import { Position } from "./game-state";

export function calcDistance(a: Position, b: Position) {
  let x = a.x - b.x;
  let y = a.y - b.y;
  return Math.sqrt(x * x + y * y);
}

export function compare(a, b) {
  return a < b ? -1 : a > b ? 1 : 0;
}