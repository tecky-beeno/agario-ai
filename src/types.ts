export type Cell = {
  x: number;
  y: number;
  radius: number;
  color: string;
  alive:boolean
};

export type Food = Cell;
export type NPC = Cell;
export type AI = Cell;
export type Player = Cell;
